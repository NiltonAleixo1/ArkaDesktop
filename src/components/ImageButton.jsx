// src/components/ImageButton.jsx
import React from 'react';
import './ImageButton.css';

const ImageButton = ({ defaultImage, selectedImage, onClick, isSelected }) => {
  return (
    <button className={`image-button ${isSelected ? 'selected' : ''}`} onClick={onClick}>
      <img src={isSelected ? selectedImage : defaultImage} alt="Botão" />
    </button>
  );
};

export default ImageButton;
