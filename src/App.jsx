// src/App.jsx
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import ABCDigital from './pages/ABCDigital';
import AprendizDigital from './pages/AprendizDigital';
import Sabichinhos from './pages/Sabichinhos';
import CodigoEspacial from './pages/CodigoEspacial';
import ABCDigitalRecursos from './pages/ABCDigital/ABCDigitalRecursos';

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/ABCDigital" element={<ABCDigital />} />
        <Route path="/AprendizDigital" element={<AprendizDigital />} />
        <Route path="/Sabichinhos" element={<Sabichinhos />} />
        <Route path="/CodigoEspacial" element={<CodigoEspacial />} />
        <Route path="/ABCDigitalRecursos" element={<ABCDigitalRecursos />} /> {/* Adicione esta linha */}
        {/* ... outras rotas ... */}
        <Route path="/Professor" component={() => {
          window.location.href = 'https://portaldoprofessor.aprendersemfronteiras.com.br/login';
          return null;
        }} />
      </Routes>
    </Router>
  );
};

export default App;
