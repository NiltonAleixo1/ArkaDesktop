// src/database.js
const { Sequelize } = require('sequelize');
const path = require('path');

const dbPath = path.resolve(__dirname, 'base', 'SFDB.db');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: dbPath,
});

// Exporta o objeto Sequelize diretamente
module.exports = sequelize;
