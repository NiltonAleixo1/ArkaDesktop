// src/Sabichinhos/SabichinhosPage.js
import React from 'react';
//import './App.css';

const SabichinhosPage = () => {
  const goToNextPage = () => {
    // Lógica para navegar para a próxima página
    // Substitua isso pela lógica real da sua aplicação
  };

  return (
    <div className="sabichinhos-container">
      <h1>Página Sabichinhos</h1>
      <p>Conteúdo da página Sabichinhos aqui.</p>
      <button onClick={goToNextPage}>Ir para a próxima página</button>
    </div>
  );
}

export default SabichinhosPage;
