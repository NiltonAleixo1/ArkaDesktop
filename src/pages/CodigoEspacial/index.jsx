// src/CodigoEspacial/CodigoEspacialPage.js
import React from 'react';
//import './App.css';

const CodigoEspacialPage = () => {
  const goToNextPage = () => {
    // Lógica para navegar para a próxima página
    // Substitua isso pela lógica real da sua aplicação
  };

  return (
    <div className="CodigoEspacial-container">
      <h1>Página CodigoEspacial</h1>
      <p>Conteúdo da página CodigoEspacial aqui.</p>
      <button onClick={goToNextPage}>Ir para a próxima página</button>
    </div>
  );
}

export default CodigoEspacialPage;
