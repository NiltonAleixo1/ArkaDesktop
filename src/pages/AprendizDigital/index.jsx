// src/Sabichinhos/AprendizDigitalPage.js
import React from 'react';
//import './App.css';

const AprendizDigitalPage = () => {
  const goToNextPage = () => {
    // Lógica para navegar para a próxima página
    // Substitua isso pela lógica real da sua aplicação
  };

  return (
    <div className="AprendizDigital-container">
      <h1>Página AprendizDigital</h1>
      <p>Conteúdo da página AprendizDigital aqui.</p>
      <button onClick={goToNextPage}>Ir para a próxima página</button>
    </div>
  );
}

export default AprendizDigitalPage;
