// src/pages/Home.js
import React, { useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import './Home.css';

const HomePage = () => {
  const containerRef = useRef(null);

  useEffect(() => {
    const handleMouseMove = (e) => {
      if (containerRef.current) {
        const { clientX } = e;
        const { offsetWidth } = containerRef.current;

        const percentageX = (clientX / offsetWidth) * 100;

        const newPosition = `${percentageX}% 50%`; // Ajuste apenas na coordenada X
        containerRef.current.style.backgroundPosition = newPosition;
      }
    };

    document.addEventListener('mousemove', handleMouseMove);

    return () => {
      document.removeEventListener('mousemove', handleMouseMove);
    };
  }, []);

  return (
    <div ref={containerRef} className="home-container">
      <div className="home-icon">
        <Link to="/ABCDigital">
          <img src="/images/ABCDigital.png" alt="ABCDigital" />
        </Link>
      </div>
      <div className="home-icon">
        <Link to="/AprendizDigital">
          <img src="/images/AprendizDigital.png" alt="AprendizDigital" />
        </Link>
      </div>
      <div className="home-icon">
        <Link to="/Sabichinhos">
          <img src="/images/Sabichinhos.png" alt="Sabichinhos" />
        </Link>
      </div>
      <div className="home-icon">
        <Link to="/CodigoEspacial">
          <img src="/images/CodigoEspacial.png" alt="CodigoEspacial" />
        </Link>
      </div>
      <div className="home-icon">
        <Link to="/Professor">
          <img src="/images/Professor.png" alt="Professor" />
        </Link>
      </div>
    </div>
  );
};

export default HomePage;
