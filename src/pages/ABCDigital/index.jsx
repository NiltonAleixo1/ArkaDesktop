// src/Sabichinhos/ABCDigitalPage.jsx
import React from 'react';
import './ABCDigital.css'; // Importe o arquivo CSS específico para esta página
// Substitua a importação de useHistory por useNavigate
import { useNavigate } from 'react-router-dom';

const ABCDigitalPage = () => {
  const navigate = useNavigate();

  const goToHomePage = () => {
    navigate('/'); // Navegar de volta para a página inicial
  };

  const goToABCDigitalRecursosPage = () => {
    navigate('/ABCDigitalRecursos'); // Navegar para a página ABCDigitalRecursos
  };

  return (
    <div className="ABCDigital-container">
      <h1>Página ABCDigital</h1>
      <p>Conteúdo da página ABCDigital aqui.</p>
      <button onClick={goToHomePage}>Voltar para o Início</button>
      <button onClick={goToABCDigitalRecursosPage}>Ir para ABCDigitalRecursos</button>
    </div>
  );
}

export default ABCDigitalPage;