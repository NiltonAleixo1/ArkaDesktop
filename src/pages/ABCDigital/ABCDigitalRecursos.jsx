// src/pages/ABCDigital/ABCDigitalRecursos.jsx
import React, { useState, useEffect } from 'react';
import './ABCDigital.css';
import ImageButton from '../../components/ImageButton';
import { ipcRenderer } from 'electron';
import { FaSearch } from 'react-icons/fa';

const ABCDigitalRecursos = () => {
    const [data, setData] = useState(null);
    const [selectedButton, setSelectedButton] = useState(null);
    const [searchTerm, setSearchTerm] = useState('');

    const buttons = [
        {
            defaultImage: '../public/images/ABCDigital/btAreaPrimeirosCliques01.png',
            selectedImage: '../public/images/ABCDigital/btAreaPrimeirosCliques02.png',
        },
        {
            defaultImage: '../public/images/ABCDigital/btAreaDesafios01.png',
            selectedImage: '../public/images/ABCDigital/btAreaDesafios02.png',
        },
        {
            defaultImage: '../public/images/ABCDigital/btAreaMaosAObra01.png',
            selectedImage: '../public/images/ABCDigital/btAreaMaosAObra02.png',
        },
        {
            defaultImage: '../public/images/ABCDigital/btAreaSupergames01.png',
            selectedImage: '../public/images/ABCDigital/btAreaSupergames02.png',
        },
        {
            defaultImage: '../public/images/ABCDigital/btAreaPainelInterativo01.png',
            selectedImage: '../public/images/ABCDigital/btAreaPainelInterativo02.png',
        },
        {
            defaultImage: '../public/images/ABCDigital/btAreaOffice01.png',
            selectedImage: '../public/images/ABCDigital/btAreaOffice02.png',
        },
        {
            defaultImage: '../public/images/ABCDigital/btAreaLetsGo01.png',
            selectedImage: '../public/images/ABCDigital/btAreaLetsGo02.png',
        },
        {
            defaultImage: '../public/images/ABCDigital/btAreaTaLigado01.png',
            selectedImage: '../public/images/ABCDigital/btAreaTaLigado02.png',
        },
        {
            defaultImage: '../public/images/ABCDigital/btAreaFerramentas01.png',
            selectedImage: '../public/images/ABCDigital/btAreaFerramentas02.png',
        },
        {
            defaultImage: '../public/images/ABCDigital/btAreaDigiteca01.png',
            selectedImage: '../public/images/ABCDigital/btAreaDigiteca02.png',
        },
        // Adicione outros botões conforme necessário
    ];

    useEffect(() => {
        ipcRenderer.invoke('query-database', 'SELECT * FROM sua_tabela')
            .then(result => setData(result));
    }, []);

    const handleButtonClick = (index) => {
        if (selectedButton === index) {
            setSelectedButton(null);
        } else {
            setSelectedButton(index);
            // Lógica para lidar com o clique do botão
        }
    };

    const handleSearchChange = (event) => {
        setSearchTerm(event.target.value);
        // Lógica para lidar com a mudança na barra de pesquisa
    };

    const handleSearchClick = async () => {
        // Lógica para lidar com o clique no botão de pesquisa
        try {
            const searchResult = await ipcRenderer.invoke('search-database', searchTerm);
            // Faça algo com os resultados da pesquisa
        } catch (error) {
            console.error('Erro ao executar a pesquisa:', error);
        }
    };

    return (
        <div className="ABCDigitalRecursos-container">
            <div className="image-button-container">
                {buttons.map((button, index) => (
                    <ImageButton
                        key={index}
                        defaultImage={button.defaultImage}
                        selectedImage={button.selectedImage}
                        onClick={() => handleButtonClick(index)}
                        isSelected={selectedButton === index}
                    />
                ))}
            </div>
            <div className="search-bar">
                <input
                    type="text"
                    placeholder="Pesquisar por nome"
                    value={searchTerm}
                    onChange={handleSearchChange}
                />
                <button onClick={handleSearchClick}>Pesquisar</button>
            </div>
        </div>
    );
};

export default ABCDigitalRecursos;