const { app, BrowserWindow, ipcMain } = require('electron');

const path = require('path');

const dbPath = path.resolve(__dirname, 'base', 'SFDB.db');
const db = new Database(dbPath);


const sequelize = require('./database');

ipcMain.handle('query-database', async (event, query) => {
  const result = await sequelize.query(query);
  return result;
});

// Resto do código para criar a janela do navegador...


function createWindow() {
    const win = new BrowserWindow({
        width: 1920,
        height: 1080,
        webPreferences: {
            nodeIntegration: true
        },
        // autoHideMenuBar: true, // Mantenha esta linha para esconder a barra de menus
        // frame: true, // Deixe frame como true para manter a barra de título
    });

    // Carregar o ícone do favicon
    const faviconPath = path.join(__dirname, 'favicon.ico');
    win.setIcon(faviconPath);

    win.loadURL("http://localhost:5173/");
}

app.whenReady().then(() => {
    createWindow();

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin')
        app.quit();
});
